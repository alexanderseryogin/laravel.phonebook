<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function search(Request $request)
    {
        return $this->userService->search($request->all());
    }
}
