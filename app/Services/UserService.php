<?php


namespace App\Services;


use App\User;
use Illuminate\Http\Request;

class UserService
{
    public function fetch()
    {
        return User::all();
    }

    public function search(array $data)
    {
        $q = $data['q'];
        return User::where('id', 'like' , '%'.$q.'%')
            ->orWhere('first_name', 'like', '%'.$q.'%')
            ->orWhere('last_name', 'like', '%'.$q.'%')
            ->orWhere('email', 'like', '%'.$q.'%')
            ->orWhere('phone', 'like', '%'.$q.'%')
            ->get();
    }

}
