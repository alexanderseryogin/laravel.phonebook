export default {
    namespaced: true,
    state: {
        users: []
    },
    actions: {
        getUsers() {
            alert(123)
            axios.get('/api/users').then(response => {
                console.log(response.data)
                // commit('SET_USERS', response.data)
            })
        }
    },
    mutations: {
        SET_USERS(state, payload) {
            state.users = payload
        }
    },
    getters: {
        users: state => {
            return state.users
        }

    }
}
