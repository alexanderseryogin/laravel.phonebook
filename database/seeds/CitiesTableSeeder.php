<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'country_id' => 3,
                'name' => 'London',
                'created_at' => '2020-06-01 13:51:52',
                'updated_at' => '2020-06-01 13:51:55',
            ),
            1 => 
            array (
                'id' => 2,
                'country_id' => 1,
                'name' => 'Washington',
                'created_at' => '2020-06-01 13:52:05',
                'updated_at' => '2020-06-01 13:52:06',
            ),
            2 => 
            array (
                'id' => 3,
                'country_id' => 2,
                'name' => 'Kiev',
                'created_at' => '2020-06-01 13:52:11',
                'updated_at' => '2020-06-01 13:52:12',
            ),
        ));
        
        
    }
}