<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'USA',
                'iso' => 'usa',
                'created_at' => '2020-06-01 13:51:20',
                'updated_at' => '2020-06-01 13:51:21',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Ukraine',
                'iso' => 'ua',
                'created_at' => '2020-06-01 13:51:27',
                'updated_at' => '2020-06-01 13:51:28',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'United Kingdom',
                'iso' => 'uk',
                'created_at' => '2020-06-01 13:51:42',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}