<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'John',
                'last_name' => 'Doe',
                'phone' => '+380991234567',
                'email' => 'johndoe@mail.com',
                'email_verified_at' => NULL,
                'password' => '',
                'remember_token' => NULL,
                'created_at' => '2020-06-01 00:00:00',
                'updated_at' => '2020-06-01 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'John1',
                'last_name' => 'Doe1',
                'phone' => '+3809932165487',
                'email' => 'john1doe1@mail.com',
                'email_verified_at' => NULL,
                'password' => '',
                'remember_token' => NULL,
                'created_at' => '2020-06-01 00:00:00',
                'updated_at' => '2020-06-01 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'first_name' => 'Vasya',
                'last_name' => 'Pupkin',
                'phone' => '+3809932165484',
                'email' => 'vasyapupkin@mail.com',
                'email_verified_at' => NULL,
                'password' => '',
                'remember_token' => NULL,
                'created_at' => '2020-06-01 00:00:00',
                'updated_at' => '2020-06-01 00:00:00',
            ),
        ));
        
        
    }
}